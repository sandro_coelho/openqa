package org.aksw.openqa.component.ui.render.impl;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletOutputStream;

import org.aksw.openqa.DefaultFrameworkParams;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.providers.impl.RenderProvider;
import org.aksw.openqa.component.ui.render.IDGenerator;
import org.aksw.openqa.component.ui.render.InfoNode;
import org.aksw.openqa.util.JSONUtil;
import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class DBpediaRender extends BasicInfoGraph {	
	
	public DBpediaRender(Map<String, Object> params) {
		super(DBpediaRender.class, params);
	}
	
	private String getResourceLoader(String resource, long labelID, long commentID, long pageID, long imageID) throws IOException, JSONException {		
		String jsonFile = resource.toString().replace("resource", "data");
		
		return "\n<script><!--\n" +
				"(function() {" +
					"var jsonFile = \"" + jsonFile + ".json\";" +
					"jQuery.getJSON(jsonFile, function(data) {" +
					"var h = (data[\"" + resource + "\"][\"http://xmlns.com/foaf/0.1/homepage\"]);" +
					"var d = (data[\"" + resource + "\"][\"http://xmlns.com/foaf/0.1/depiction\"]);" +
					"if(isDefined(h)){document.getElementById(\"page" + pageID + "\").innerHTML = h[0].value;" +
					"document.getElementById(\"page" + pageID + "\").href = h[0].value;}" +
					"if(isDefined(d)){var picture = d[0].value;" +
					//"picture = picture.replace('commons','en');" +
					//"jQuery( \"<img/>\" ).attr( \"src\", picture ).css( \"height\", \"auto\" ).css( \"width\", 50 ).appendTo( \"#image" + imageID + "\" );}" +
					"jQuery( \"<img/>\" ).attr( \"src\", picture ).css( \"height\", \"auto\" ).css( \"width\", 50 ).error(function(){picture = picture.replace('commons','en');jQuery( this ).attr( \"src\", picture );}).appendTo( \"#image" + imageID + "\" );}" +
				"});})();\n//-->" +
			"</script>\n";
	}
	
	public static boolean exists(String URLName){
	    try {
	      HttpURLConnection.setFollowRedirects(false);
	      // note : you may also need
	      //        HttpURLConnection.setInstanceFollowRedirects(false)
	      HttpURLConnection con =
	         (HttpURLConnection) new URL(URLName).openConnection();
	      con.setRequestMethod("HEAD");
	      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       return false;
	    }
	  }  
	
	@Override
	public void printContent(RenderProvider render, InfoNode result, Context context, ServletOutputStream out) throws Exception {		
		String output = "";
		Entry<String, Object> entry = result.getResult().getParams().entrySet().iterator().next();
		String type = entry.getKey();
		Object value = (String) entry.getValue();
		long contentID = IDGenerator.getInstance().generateID();
		if(type == DefaultFrameworkParams.RESOURCE_PARAM || type == DefaultFrameworkParams.URI_PARAM) {
			String resource = StringEscapeUtils.escapeHtml(value.toString());
			source = "http://wikipedia.org/wiki/" + resource.substring(resource.lastIndexOf("/") + 1);
			String jsonFile = resource.toString().replace("resource", "data") + ".json";
			String label = "undefined";
			String comment = "no comment";

			long imageID = IDGenerator.getInstance().generateID();
			long labelID = IDGenerator.getInstance().generateID();
			long pageID = IDGenerator.getInstance().generateID();
			long commentID = IDGenerator.getInstance().generateID();

			try {
				JSONObject object = JSONUtil.read(jsonFile);
				
				JSONObject resourceObject = object.getJSONObject(resource);
				label = "undefined";
				org.json.JSONArray labels = null;
				if(resourceObject.has("http://dbpedia.org/property/fullname")) {
					labels = resourceObject.getJSONArray("http://dbpedia.org/property/fullname");
				} else if(resourceObject.has("http://dbpedia.org/property/name")) {
					labels = resourceObject.getJSONArray("http://dbpedia.org/property/name");
				} else if(resourceObject.has("http://www.w3.org/2000/01/rdf-schema#label"))
					labels = resourceObject.getJSONArray("http://www.w3.org/2000/01/rdf-schema#label");
				
				if(labels != null)
					label = getLang(labels, "value", "en");				
				
				if(resourceObject.has("http://www.w3.org/2000/01/rdf-schema#comment")) {
					org.json.JSONArray comments = resourceObject.getJSONArray("http://www.w3.org/2000/01/rdf-schema#comment");
					if(comments != null)
						comment = getLang(comments, "value", "en");
				}
			} catch (Exception e) {
				label = "Warning";
				comment = "Problem retrieving content.";
			}			
			
			output = "<h3 id=\"label" + labelID + "\" style=\"margin: 5px;\">"+ StringEscapeUtils.escapeHtml(label) +"</h3>";
			
			output +=
			"<div id=\"image" + imageID + "\" style=\"float:left;margin: 0 10 10 10;\"></div>" +
			"<div id=\"content" +  contentID + "\" style=\"text-align: left;height: 60px;\">" +
				"<div id =\"comment" + commentID + "\">" +  StringEscapeUtils.escapeHtml(comment) + "</div>" +
				"<a id =\"page" + pageID + "\" style=\"font-size: small;\" href=\"\" target=\"_blank\"></a>";
			
			output += getResourceLoader(resource, labelID, commentID, pageID, imageID);
			output += "</div>"; // closing content div
		} else if (value != null) {
			output = "<div id=\"content" +  contentID + "\" style=\"text-align: left;height: 60px;font-size: xx-large;\">";
			output +=  value;// resource.asLiteral().getValue();
			output += "</div>"; // closing content div
		}
		
		out.print(output);
	}
	
	@Override
	public boolean canRender(Context context, InfoNode infoNode)
			throws Exception {
		IResult result = infoNode.getResult();
		return result.contains(DefaultFrameworkParams.URI_PARAM) ||
				result.contains(DefaultFrameworkParams.LITERAL_NUMBER_PARAM) ||
				result.contains(DefaultFrameworkParams.LITERAL_DATE_PARAM) ||
				result.contains(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM) ||
				result.contains(DefaultFrameworkParams.RESOURCE_PARAM);
	}
	
	private String getLang(org.json.JSONArray array, String attribute, String lang) throws JSONException {
		for(int i=0; i < array.length(); i++) {
			JSONObject obj = array.getJSONObject(i);
			if(obj.has("lang")) {
				if(obj.getString("lang").equals(lang))
					return obj.getString(attribute);
			}
		}
		return null;
	}
	
	@Override
	protected void printEnd(RenderProvider render, InfoNode infoNode, Context context, ServletOutputStream out) throws Exception {
		String resource = (String) infoNode.getResult().getParam(DefaultFrameworkParams.URI_PARAM);
		if(resource != null) {
			resource = StringEscapeUtils.escapeHtml(resource);
			String source = "http://wikipedia.org/wiki/" + resource.substring(resource.toString().lastIndexOf("/") + 1);
			if(source != null) {
				out.print("<div style=\"height:10px;\" ><a target=\"_blank\" style=\"float:right;font-size:smaller;\" href=\"" + source + "\"><span>" + source + "</span></a></div>");
			}
		}		
		out.print(endHTML);
	}
}
