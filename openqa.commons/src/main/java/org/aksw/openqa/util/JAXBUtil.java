package org.aksw.openqa.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

public class JAXBUtil {

	public static <E> E deserialize(Class<E> clazz, File file) throws JAXBException, FileNotFoundException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		@SuppressWarnings("unchecked")
		E instance = (E) jaxbUnmarshaller.unmarshal(file);
		return instance;
	}
	
	public static <E> void serialize(Class<E> clazz, E instance, OutputStream out, String[] cdataElements) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
		
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		XMLSerializer serializer = getXMLSerializer(out, cdataElements);
		jaxbMarshaller.marshal(instance, serializer);
	}
	
	private static XMLSerializer getXMLSerializer(OutputStream out, String[] cdataElements) {
        // configure an OutputFormat to handle CDATA
        OutputFormat of = new OutputFormat();
        
        // specify which of your elements you want to be handled as CDATA.
        // The use of the '^' between the namespaceURI and the localname
        // seems to be an implementation detail of the xerces code.
        // When processing xml that doesn't use namespaces, simply omit the
        // namespace prefix as shown in the third CDataElement below.
        of.setCDataElements(cdataElements);            
        of.setIndenting(true);
        // create the serializer
        XMLSerializer serializer = new XMLSerializer(of);
        serializer.setOutputByteStream(out);

        return serializer;
    }

}
