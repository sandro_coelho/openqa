package org.aksw.openqa.component.answerformulation.retriever.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.aksw.openqa.DefaultFrameworkParams;
import org.aksw.openqa.component.answerformulation.AbstractRetriever;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Result;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.util.SPARQLUtil;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import com.hp.hpl.jena.sparql.syntax.ElementGroup;
import com.hp.hpl.jena.sparql.syntax.ElementPathBlock;

public class TripleStoreRetriever extends AbstractRetriever {
	
	private static Logger logger = Logger.getLogger(TripleStoreRetriever.class);
	
	// token 
	public static final String END_POINT_PARAM = "END_POINT";
	public static final String DEFAULT_GRAPH_PARAM = "DEFAULT_GRAPH";
	
	public static final String SPARQL_PARAM = "SPARQL_PARAM";
	
	public TripleStoreRetriever(Map<String, Object> params) {
		super(TripleStoreRetriever.class, params);
	}

	@Override
	public boolean canProcess(IParam token) {
		return token.contains(SPARQL_PARAM);
	}

	@Override
	public List<? extends IResult> process(IParam param, ServiceProvider serviceProvider, Context context) {
		String sparqlParam = (String) param.getParam(SPARQL_PARAM);
		
		String graph = (String) getParam(DEFAULT_GRAPH_PARAM);
		String endPoint = (String) getParam(END_POINT_PARAM);
		
		return getSPARQLQueryResult(sparqlParam, endPoint, graph);
	}
	
	public List<Result> getSPARQLQueryResult(String sparqlQuery, String endPoint, String defaultGraphString) {
		List<Result> results = new ArrayList<Result>();
		
		// if there is no ASK and LIMIT placed in query
		if(!sparqlQuery.toLowerCase().contains("ask") && !sparqlQuery.toLowerCase().contains("limit"))
			sparqlQuery= sparqlQuery + " limit 10 "; // add LIMIT
		
		logger.debug("Processing query " + sparqlQuery);
		Query query = QueryFactory.create(sparqlQuery);
		boolean isQueryStar = SPARQLUtil.isSingleEntity(query);
		if(!isQueryStar) {
			QueryEngineHTTP qexec = new QueryEngineHTTP(endPoint, query);
			qexec.setTimeout(30000, 30000);
			qexec.addDefaultGraph(defaultGraphString);
			try {
			    ResultSet rs = qexec.execSelect();
				List<String> headers = rs.getResultVars();
				while(rs.hasNext()) {
			    	QuerySolution  solution=rs.nextSolution();
					for(String p : headers) {
						Result result = new Result();
						RDFNode node = solution.get(p);
						if(node.isURIResource()) {
							String value = node.toString();
							result.setParam(DefaultFrameworkParams.URI_PARAM, value);
						} else if (node.isLiteral()) {
							Object value = node.asLiteral().getValue();
							if(value instanceof Number)
								result.setParam(DefaultFrameworkParams.LITERAL_NUMBER_PARAM, value);
							else if (value instanceof Date)
								result.setParam(DefaultFrameworkParams.LITERAL_DATE_PARAM, value);
							else if(value instanceof Boolean)
								result.setParam(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM, value);			
						} else if (node.isResource()) {
							String value = (String) node.asResource().getURI();
							result.setParam(DefaultFrameworkParams.RESOURCE_PARAM, value);
						}
						results.add(result);
		        	}
				}			
			} catch(Exception e) {
				logger.error("Error executing query: " + sparqlQuery,  e);
			} finally {
				qexec.close();
			}
		} else {
			ElementGroup elementGroup  = (ElementGroup) query.getQueryPattern();
			ElementPathBlock elementTripleBlock = (ElementPathBlock) elementGroup.getElements().get(0);
			String value = elementTripleBlock.getPattern().get(0).getSubject().toString();
			Result result = new Result();
			result.setParam(DefaultFrameworkParams.RESOURCE_PARAM, value);
			results.add(result);			
		}
		return results;
	}

}
