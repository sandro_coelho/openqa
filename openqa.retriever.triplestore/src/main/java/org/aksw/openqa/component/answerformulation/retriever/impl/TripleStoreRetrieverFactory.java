package org.aksw.openqa.component.answerformulation.retriever.impl;

import java.util.Map;

import org.aksw.openqa.component.answerformulation.AbstractRetrieverFactorySpi;
import org.aksw.openqa.component.answerformulation.Retriever;

public class TripleStoreRetrieverFactory extends AbstractRetrieverFactorySpi {
	@Override
	public Retriever create(Map<String, Object> params) {
		return create(TripleStoreRetriever.class, params);
	}
}
