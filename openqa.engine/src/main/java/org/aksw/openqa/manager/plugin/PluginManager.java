package org.aksw.openqa.manager.plugin;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IProvider;
import org.aksw.openqa.component.providers.impl.ContextProvider;
import org.aksw.openqa.component.providers.impl.InterpreterProvider;
import org.aksw.openqa.component.providers.impl.RenderProvider;
import org.aksw.openqa.component.providers.impl.ResolverProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.util.PropertyLoaderUtil;
import org.apache.log4j.Logger;

public class PluginManager extends PropertyLoaderUtil {
	
	private static Logger logger = Logger.getLogger(PluginManager.class);
	
	private final static String PLUGIN_PATH = "plugins";
	
	private File plugginDir = new File(PLUGIN_PATH);
	private ClassLoader parent;
	private boolean load = true;
	
	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = new HashMap<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>>();
	
	public PluginManager(String plugginDir, ClassLoader parent) {
		this.plugginDir = new File(plugginDir);
		this.parent = parent;
	}
	
	public PluginManager(ClassLoader parent) {
		this.parent = parent;
	}
	
	public void load() {
        List<ClassLoader> classLoaders = new ArrayList<ClassLoader>();
        
        // add the application directory as class path to look for plugins in main class path
        classLoaders.add(parent);
        
        if(plugginDir != null) {        
	        File[] directories = plugginDir.listFiles(new FileFilter() {
	          @Override
	          public boolean accept(File file) {
	            return file.isDirectory();
	          }
	        });        
	        
	        if(directories != null) {
		        for(File directory : directories) {
		           File[] flist = directory.listFiles(new FileFilter() {
		                public boolean accept(File file) {return file.getPath().toLowerCase().endsWith(".jar");}
		           });
		
		           URL[] urls = new URL[flist.length];
		           for (int i = 0; i < flist.length; i++) {
		        	   try {
							urls[i] = flist[i].toURI().toURL();
		        	   } catch (MalformedURLException e) {
							logger.error("Error in URI format", e);
		        	   }
		           }
		           
		           if(urls.length > 0 ) {
		        	   URLClassLoader ucl = new URLClassLoader(urls, parent);
		        	   classLoaders.add(ucl);
		           }
		        }
	        }
        }
        
        ServiceProvider serviceProvider = new ServiceProvider(classLoaders);
        providers.put(ServiceProvider.class, serviceProvider);
        
        InterpreterProvider interpreterProvider = new InterpreterProvider(classLoaders, serviceProvider);
        providers.put(InterpreterProvider.class, interpreterProvider);
        
        RetrieverProvider retrieverProvider = new RetrieverProvider(classLoaders, serviceProvider);
        providers.put(RetrieverProvider.class, retrieverProvider);
        
        SynthesizerProvider synthesizerProvider = new SynthesizerProvider(classLoaders, serviceProvider);
        providers.put(SynthesizerProvider.class, synthesizerProvider);
        
        ResolverProvider resolverProvider = new ResolverProvider(classLoaders, serviceProvider);
        providers.put(ResolverProvider.class, resolverProvider);
        
        RenderProvider renderProvider = new RenderProvider(classLoaders, serviceProvider);
        providers.put(RenderProvider.class, renderProvider);
        
        ContextProvider contextProvider = new ContextProvider(classLoaders, serviceProvider);
        providers.put(ContextProvider.class, contextProvider);
	}
	
    public static void main(String[] args) throws IOException {
    	ClassLoader cl = PluginManager.class.getClassLoader();
        PluginManager pluggins = new PluginManager(PLUGIN_PATH, cl);
        pluggins.load();
    }
        
    public List<? extends IPlugin> getPlugins(Class<? extends IProvider<? extends IPlugin>> providerClass) throws Exception {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	IProvider<? extends IPlugin> provider = providers.get(providerClass);
    	return provider.list();
    }
    
    public IPlugin getPlugin(String id)  {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	for(IProvider<? extends IPlugin> providerEntry : providers.values()) {
    		 IPlugin component = providerEntry.get(id);
    		if(component != null)
    			return component;
    	}    	
    	return null;
    }
    
    public <C extends IPlugin> C getPluin(Class<? extends IProvider<?>> providerClass, Class<C> componentClass) {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	@SuppressWarnings("unchecked")
		IProvider<C> provider = (IProvider<C>) providers.get(providerClass);
    	if(provider != null)
			return provider.get(componentClass);
    	return null;
    }
    
    public <C extends IPlugin> IProvider<? extends C> getProvider(Class<? extends IProvider<C>> providerClass) throws Exception {
    	Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = getProviders();
    	@SuppressWarnings("unchecked")
		IProvider<C> provider = (IProvider<C>) providers.get(providerClass);
		return provider;
    }
    
	private Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> getProviders() {
		if(load) {
			load();
			load = false;
		}
		return providers;
	}

	public void setActivate(boolean activate) {
		Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providersMap = getProviders();
		Collection<IProvider<? extends IPlugin>> providers = providersMap.values();
		for(IProvider<? extends IPlugin> provider : providers) {
			List<? extends IPlugin> plugins = provider.list();
			for(IPlugin plugin : plugins) {
				plugin.setActive(activate);
			}
		}
	}
}