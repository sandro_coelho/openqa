package org.aksw.openqa;

public class DefaultFrameworkParams {
	
	// Param types
	public final static String URI_PARAM = "URL_PARAM";
	public final static String RESOURCE_PARAM = "RESOURCE_PARAM";
	public final static String SPARQL_PARAM = "SPARQL_PARAM";

	// Param Literal types
	public final static String LITERAL_NUMBER_PARAM = "LITERAL_NUMBER_PARAM";
	public final static String LITERAL_DATE_PARAM = "LITERAL_DATE_PARAM";
	public final static String LITERAL_BOOLEAN_PARAM = "LITERAL_BOOLEAN_PARAM";

	/**
	 * List of user input params
	 */
	public final static String USER_PARAMS = "USER_INPUT_PARAMS";
	public static final String NL_INPUT_PARAM = "NL_INPUT_PARAM";
}
