package org.aksw.openqa.component.context;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractContextFactorySpi extends AbstractPluginFactorySpi<Context> implements ContextFactorySpi {
}
