package org.aksw.openqa.component.providers.impl;
import java.util.List;

import org.aksw.openqa.component.ProcessProvider;
import org.aksw.openqa.component.answerformulation.Interpreter;
import org.aksw.openqa.component.answerformulation.InterpreterFactorySpi;
 
public class InterpreterProvider extends ProcessProvider<Interpreter, InterpreterFactorySpi> {
    public InterpreterProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(InterpreterFactorySpi.class, classLoaders, serviceProvider);
	}
}