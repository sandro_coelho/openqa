package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.IPluginFactorySpi;

public interface SynthesizerFactorySpi extends IPluginFactorySpi<Synthesizer> {

}
