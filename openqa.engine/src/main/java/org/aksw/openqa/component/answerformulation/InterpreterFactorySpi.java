package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.IPluginFactorySpi;

public interface InterpreterFactorySpi extends IPluginFactorySpi<Interpreter> {
}
