package org.aksw.openqa.component.answerformulation.synthesizer.impl;

import java.util.Map;

import org.aksw.openqa.component.answerformulation.AbstractSynthesizerFactorySpi;
import org.aksw.openqa.component.answerformulation.Synthesizer;

public class DefaultSynthesizerFactory extends AbstractSynthesizerFactorySpi {
	@Override
	public Synthesizer create(Map<String, Object> params) {
		return create(DefaultRDFTermSynthesizer.class, params);
	}
}
