package org.aksw.openqa.component.service;

import org.aksw.openqa.component.IPluginFactorySpi;

public interface ServiceFactorySpi extends IPluginFactorySpi<Service> {
}
