package org.aksw.openqa.component.context;

import java.util.Map;

import org.aksw.openqa.component.AbstractPlugin;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractContext extends AbstractPlugin implements Context {
	public AbstractContext(Class<? extends IPlugin> c, Map<String, Object> params) {
		super(c, params);
	}	
}
