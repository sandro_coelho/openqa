package org.aksw.openqa.component.providers.impl;

import java.util.List;

import org.aksw.openqa.component.ProcessProvider;
import org.aksw.openqa.component.answerformulation.Resolver;
import org.aksw.openqa.component.answerformulation.ResolverFactorySpi;

public class ResolverProvider extends ProcessProvider<Resolver, ResolverFactorySpi> {
	public ResolverProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(ResolverFactorySpi.class, classLoaders, serviceProvider);
	}
}
