package org.aksw.openqa.component.providers.impl;

import java.util.List;

import org.aksw.openqa.component.AbstractPluginProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.context.ContextFactorySpi;

public class ContextProvider extends AbstractPluginProvider<Context, ContextFactorySpi> {
    public ContextProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(ContextFactorySpi.class, classLoaders, serviceProvider);
	}
}
