package org.aksw.openqa.component.ui.render;

import org.aksw.openqa.component.IPluginFactorySpi;

public interface RenderFactorySpi extends IPluginFactorySpi<InfoGraphRender> {
}
