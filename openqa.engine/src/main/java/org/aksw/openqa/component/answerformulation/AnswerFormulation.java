package org.aksw.openqa.component.answerformulation;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IPluginFactorySpi;
import org.aksw.openqa.component.ProcessProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Param;
import org.aksw.openqa.component.providers.impl.InterpreterProvider;
import org.aksw.openqa.component.providers.impl.ResolverProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.apache.log4j.Logger;

public class AnswerFormulation {
	
	private static Logger logger = Logger.getLogger(AnswerFormulation.class);

	public List<? extends IResult> process(boolean skipRetriever, Map<String, Object> params,
			PluginManager pluginManager,
			Context context) throws Exception {

		ServiceProvider serviceProvider = (ServiceProvider) pluginManager.getProvider(ServiceProvider.class);
		Param token = new Param();
		
		logger.info("Parameterizing");
		
		// param set
		for(Entry<String, Object> entry : params.entrySet())
			token.setParam(entry.getKey(), entry.getValue());
		
		logger.debug("Interpreting");
		InterpreterProvider interpreterProvider = (InterpreterProvider) pluginManager.getProvider(InterpreterProvider.class);
		List<? extends IResult> interpretations = interpreterProvider.process(token, serviceProvider, context);
		logger.debug("Number of interpretations: " + interpretations.size());

		if(skipRetriever)
			return interpretations;
		
		logger.debug("Retrieving ");
		RetrieverProvider retrieverProvider = (RetrieverProvider) pluginManager.getProvider(RetrieverProvider.class);
		List<? extends IResult> retrieverResults = retrieverProvider.process(interpretations, serviceProvider, context);	
		logger.debug("Number of retrieved results: " + retrieverResults.size());
		
		logger.debug("Synthesizing");
		SynthesizerProvider synthesizerProvider = (SynthesizerProvider) pluginManager.getProvider(SynthesizerProvider.class);
		List<? extends IResult> synthesis = synthesizerProvider.process(retrieverResults, serviceProvider, context);
		logger.debug("Number of synthesis: " + synthesis.size());
		
		logger.debug("Resolving");
		ResolverProvider resolverProvider = (ResolverProvider) pluginManager.getProvider(ResolverProvider.class);
		List<? extends IResult> resolved = resolverProvider.process(synthesis, serviceProvider, context);
		logger.debug("Number of resolved results: " + resolved.size());
			
		return resolved;
	}
	
	public class ProcessStage {
		
		ProcessProvider<? extends IPlugin, ? extends IPluginFactorySpi<? extends IPlugin>> process;
		ServiceProvider serviceProvider;
		Context context;
		
		public ProcessStage(ProcessProvider<? extends IPlugin, 
				? extends IPluginFactorySpi<? extends IPlugin>> process,
						ServiceProvider serviceProvider,
						Context context) {
			this.process = process;
			this.context = context;
			this.serviceProvider = serviceProvider;
		}
		
		public void execute() {
			//process.process(param, serviceProvider, context);
		}
	}
}
