package org.aksw.openqa.component.answerformulation;

import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractRetriever extends AbstractQFProcessor implements Retriever {

	public AbstractRetriever(Class<? extends IPlugin> c, Map<String, Object> params) {
		super(c, params);
	}
}
