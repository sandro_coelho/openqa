package org.aksw.openqa.component;

import java.util.List;

import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.providers.impl.ServiceProvider;

public interface IProcess extends IPlugin {
	public boolean canProcess(List<? extends IParam> params);
	public boolean canProcess(IParam param);
	
	public List<? extends IResult> process(IParam param, ServiceProvider services, Context context) throws Exception;
	public List<? extends IResult> process(List<? extends IParam> params, ServiceProvider services, Context context) throws Exception;
}