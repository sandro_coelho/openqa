package org.aksw.openqa.component.object;

import java.util.Map;

import org.aksw.openqa.component.IComponent;

public class Result extends AbstractResult {
		
	IComponent source;
	IParam inputParam;

	public Result() {
	}
	
	public Result(Map<String, Object> params, IParam inputParam, IComponent source) {
		// setting the source
		this.source = source;
		this.inputParam = inputParam;
		setParams(params);
	}
	
	public Result(Map<String, Object> params) {
		// setting the source
		setParams(params);
	}
	
	public Result(IParam inputParam, IComponent source) {
		// setting the source
		this.source = source;
		this.inputParam = inputParam;
	}

	@Override
	public IComponent getSource() {
		return source;
	}

	@Override
	public IParam getInputParam() {
		return inputParam;
	}
}
