package org.aksw.openqa.component.answerformulation;

import java.io.IOException;
import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractResolver extends AbstractQFProcessor implements Resolver {

	public AbstractResolver(Class<? extends IPlugin> c, Map<String, Object> params) throws IOException {
		super(c, params);
	}

}
