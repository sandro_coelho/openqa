package org.aksw.openqa.component.service.impl;

import java.util.Map;

import org.aksw.openqa.component.service.AbstractServiceFactorySpi;
import org.aksw.openqa.component.service.Service;

public class CacheServiceFactory extends AbstractServiceFactorySpi {
	@Override
	public Service create(Map<String, Object> params) {
		return create(DefaultCacheService.class, params);
	}
}
