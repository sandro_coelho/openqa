package org.aksw.openqa.component.context;

import org.aksw.openqa.component.IPluginFactorySpi;

public interface ContextFactorySpi extends IPluginFactorySpi<Context> {
}
