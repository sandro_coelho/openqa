package org.aksw.openqa.component.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Cache {
	
	int capacity = 100;
	Map<Object, Object> objects = new HashMap<Object, Object>();

	public Cache(int capacity) throws Exception {
		if(capacity < 1)
			throw new Exception("Invalid capacity value:" + capacity);
		this.capacity = capacity;
	}
	
	public synchronized void setCapacity(int capacity) {
		int currentSize = objects.size();
		if(currentSize > capacity) {
			remove(currentSize - capacity);
		}
		this.capacity = capacity;
	}
	
	private void remove(int numberOfElements) {
		Collection<Object> values = objects.values(); 
		while(numberOfElements > 0) {
			objects.remove(values.iterator().next());
		}
	}
	
	public synchronized Object get(Object key) {
		return objects.get(key);
	}

	public synchronized void put(Object key, Object value) {
		if(objects.size() == capacity) {
			remove(1);
		}
		objects.put(key, value);
	}

}
