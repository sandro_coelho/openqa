package org.aksw.openqa.component.answerformulation;

import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractInterpreter extends AbstractQFProcessor implements Interpreter {
	public AbstractInterpreter(Class<? extends IPlugin> c, Map<String, Object> params) {
		super(c, params);
	}
}
