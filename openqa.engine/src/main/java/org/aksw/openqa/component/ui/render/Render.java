package org.aksw.openqa.component.ui.render;

import javax.servlet.ServletOutputStream;

import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.providers.impl.RenderProvider;

public interface Render {
	public void render(RenderProvider renderProvider, InfoNode infoNode, Context context, ServletOutputStream out) throws Exception;
}
