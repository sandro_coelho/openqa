package org.aksw.openqa.component.providers.impl;

import java.util.List;

import org.aksw.openqa.component.ProcessProvider;
import org.aksw.openqa.component.answerformulation.Synthesizer;
import org.aksw.openqa.component.answerformulation.SynthesizerFactorySpi;

public class SynthesizerProvider extends ProcessProvider<Synthesizer, SynthesizerFactorySpi> {
	public SynthesizerProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(SynthesizerFactorySpi.class, classLoaders, serviceProvider);
	}
}
