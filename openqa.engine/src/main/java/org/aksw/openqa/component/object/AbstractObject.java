package org.aksw.openqa.component.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class AbstractObject implements IObject {
	
	private Map<String, Object> params = new HashMap<String, Object>();

	public boolean contains(String param) {
		return params.containsKey(param);
	}
	
	public Object getParam(String param) {
		return params.get(param);
	}
	
	public void setParam(String param, Object o) {
		params.put(param, o);
	}
	
	public void setParams(Map<String, Object> entries) {
		if(entries != null) {
			for(Entry<String, Object> entry : entries.entrySet()) {
				params.put(entry.getKey(), entry.getValue());
			}
		}
	}
	
	public Map<String, Object> getParams() {
		return params;
	}
}
