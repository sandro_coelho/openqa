package org.aksw.openqa.component.answerformulation.synthesizer.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.DefaultFrameworkParams;
import org.aksw.openqa.component.answerformulation.AbstractSynthesizer;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Result;
import org.aksw.openqa.component.providers.impl.ServiceProvider;

public class DefaultRDFTermSynthesizer extends AbstractSynthesizer {

	
	public DefaultRDFTermSynthesizer(Map<String, Object> params)
			throws IOException {
		super(DefaultRDFTermSynthesizer.class, params);
	}

	Comparator<Entry<String, Candidate>> comparator = new Comparator<Entry<String, Candidate>>() {
	    public int compare(Entry<String, Candidate> e1, Entry<String, Candidate> e2) {
	    	Candidate c1 = e1.getValue();
	    	Candidate c2 = e2.getValue();
	        return c2.incidence - c1.incidence; // measuring the incidence
	    }
	};
	
	@Override
	public boolean canProcess(IParam param) {
		return param.contains(DefaultFrameworkParams.URI_PARAM) ||
						param.contains(DefaultFrameworkParams.LITERAL_NUMBER_PARAM) ||
								param.contains(DefaultFrameworkParams.LITERAL_DATE_PARAM) ||
										param.contains(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM) ||
												param.contains(DefaultFrameworkParams.RESOURCE_PARAM);
	}
	
	@Override
	public java.util.List<? extends IResult> process(java.util.List<? extends IParam> params,			ServiceProvider services, 
			Context context) throws Exception {
		Map<String, Candidate>  instanceIncidence = new HashMap<String, Candidate>();
		for(IParam param : params) {
			String attrValue = null;
			String attrKey = null;
			if(param.contains(DefaultFrameworkParams.URI_PARAM)) {
				attrValue = (String) param.getParam(DefaultFrameworkParams.URI_PARAM);
				attrKey = DefaultFrameworkParams.URI_PARAM;
			} else if (param.contains(DefaultFrameworkParams.LITERAL_NUMBER_PARAM))
				attrValue = (String) param.getParam(DefaultFrameworkParams.LITERAL_NUMBER_PARAM);
			else if (param.contains(DefaultFrameworkParams.LITERAL_DATE_PARAM))
				attrValue = (String) param.getParam(DefaultFrameworkParams.LITERAL_DATE_PARAM);
			else if (param.contains(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM))
				attrValue = (String) param.getParam(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM);
			else {
				attrValue = (String) param.getParam(DefaultFrameworkParams.RESOURCE_PARAM);
			}
			
			// there is no check for null, once canProcess assure that there is the attribute

			if(attrKey == DefaultFrameworkParams.URI_PARAM) {
				attrValue = attrValue.substring(attrKey.lastIndexOf("/") + 1);
			}
			
			if(instanceIncidence.containsKey(attrValue)) {
				Candidate candidate = instanceIncidence.get(attrValue);
				candidate.incidence++;
				instanceIncidence.put(attrValue, candidate);
			} else {
				Candidate candidate = new Candidate();
				candidate.param = param;
				candidate.incidence++;
				instanceIncidence.put(attrValue, candidate);
			}		
		}
		
		List<Entry<String, Candidate>> intanceIncidenceList = new ArrayList<Entry<String, Candidate>>(instanceIncidence.entrySet());
		Collections.sort(intanceIncidenceList, comparator);
		List<IResult> results = new ArrayList<IResult>();
		for(Entry<String, Candidate> entry: intanceIncidenceList) {			
			Candidate candidate = entry.getValue();
			IParam token = candidate.param;
			Result result = new Result(token.getParams());
			results.add(result);
		}
		
		return results;
	}

	private class Candidate {
		IParam param;
		int incidence = 0;
	}

	@Override
	public List<? extends IResult> process(IParam param,
			ServiceProvider services, Context context) throws Exception {
		List<IResult> results = new ArrayList<IResult>();
		Result result = new Result(param.getParams());
		results.add(result);
		return results;
	}
	
}
