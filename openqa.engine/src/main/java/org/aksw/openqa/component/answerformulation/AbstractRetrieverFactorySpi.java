package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractRetrieverFactorySpi extends AbstractPluginFactorySpi<Retriever> implements RetrieverFactorySpi {
}
