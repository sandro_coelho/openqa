package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractInterpreterFactorySpi extends AbstractPluginFactorySpi<Interpreter> implements InterpreterFactorySpi {
}
