package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractResolverFactorySpi extends AbstractPluginFactorySpi<Resolver> implements ResolverFactorySpi {
}
