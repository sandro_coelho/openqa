package org.aksw.openqa.component;

import java.util.ArrayList;
import java.util.List;

import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Result;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.log4j.Logger;

public class ProcessProvider<C extends IProcess, F extends IPluginFactorySpi<C>> extends AbstractPluginProvider<C, F> {
	
	private static Logger logger = Logger.getLogger(ProcessProvider.class);
	
	public ProcessProvider(Class<F> clazz, List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(clazz, classLoaders, serviceProvider);
	}
	
	protected List<? extends IResult> process(IParam param, List<C> components, ServiceProvider serviceProvider, Context context) throws Exception {
    	List<IResult> results = new ArrayList<IResult>();
    	boolean processed = false;
        for (C component : components) {
        	// if the component is active and can process the token
			if(component.isActive() && component.canProcess(param)) {
				processed = true;
				try {
					results.addAll(component.process(param, serviceProvider, context)); // process the token
				} catch (Exception e) {
					logger.error("ProcessProvider exception", e);
				} catch (Error e) {
					logger.error("ProcessProvider error", e);
				}
			}
		}        
    	// if is no processed, return the token themselves
        if(!processed) {
        	IResult result = new Result(param, this);
    		results.add(result);
    		return results; // return
        }
        return results;
    }
    
	protected List<? extends IResult> process(List<? extends IParam> params, List<C> components, ServiceProvider serviceProvider, Context context) {
    	List<IResult> results = new ArrayList<IResult>();
    	boolean processed = false;
        for (C component : components) {
        	// if the component is activated and can process the token
			if(component.isActive() && component.canProcess(params)) {
				processed = true;
				try {
					results.addAll(component.process(params, serviceProvider, context)); // process the token
				} catch (Exception e) {
					logger.error("ProcessProvider exception", e);
				} catch (Error e) {
					logger.error("ProcessProvider error", e);
				}
			}
		}
    	// if is no processed, return the token themselves
    	if(!processed) {
    		for(IParam token : params) {
	    		Result result = new Result(token.getParams(), token, this);
	    		results.add(result); // return
    		}
    		return results;
    	}
        return results;
    }
	
	public List<? extends IResult> process(IParam param, ServiceProvider services, Context context) throws Exception {
		return process(param, list(), services, context);
	}
	
	public List<? extends IResult> process(List<? extends IParam> params, ServiceProvider services, Context context) throws Exception {
		return process(params, list(), services, context);
	}
}
