package org.aksw.openqa.component.object;

public interface IResult extends IParam {
	public IParam getInputParam();
}
