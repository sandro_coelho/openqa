package org.aksw.openqa.component.service;

import java.util.Map;

import org.aksw.openqa.component.AbstractPlugin;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractService extends AbstractPlugin implements Service {

	public AbstractService(Class<? extends IPlugin> c, Map<String, Object> params) {
		super(c, params);
	}
	
}