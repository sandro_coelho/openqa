package org.aksw.openqa.component.answerformulation;

import java.io.IOException;
import java.util.Map;

import org.aksw.openqa.component.AbstractQFProcessor;
import org.aksw.openqa.component.IPlugin;

public abstract class AbstractSynthesizer extends AbstractQFProcessor implements Synthesizer {

	public AbstractSynthesizer(Class<? extends IPlugin> c, Map<String, Object> params)
			throws IOException {
		super(c, params);
	}
}
