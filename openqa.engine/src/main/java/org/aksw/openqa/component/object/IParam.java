package org.aksw.openqa.component.object;

import org.aksw.openqa.component.IComponent;


public interface IParam extends IObject {
	public IComponent getSource();
}
