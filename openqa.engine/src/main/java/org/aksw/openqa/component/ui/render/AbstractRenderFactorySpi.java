package org.aksw.openqa.component.ui.render;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractRenderFactorySpi  extends AbstractPluginFactorySpi<InfoGraphRender> implements RenderFactorySpi {
}
