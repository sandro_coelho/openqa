package org.aksw.openqa.component.context.impl;

import java.util.Map;

import org.aksw.openqa.component.context.AbstractContextFactorySpi;
import org.aksw.openqa.component.context.Context;

public class DefaultContextServiceFactory extends AbstractContextFactorySpi {
	@Override
	public Context create(Map<String, Object> params) {
		return create(DefaultContext.class, params);
	}
}
