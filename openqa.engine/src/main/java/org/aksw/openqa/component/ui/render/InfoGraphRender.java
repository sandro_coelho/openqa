package org.aksw.openqa.component.ui.render;

import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.context.Context;


public interface InfoGraphRender extends Render, IPlugin {
	public boolean canRender(Context context, InfoNode node) throws Exception;
}