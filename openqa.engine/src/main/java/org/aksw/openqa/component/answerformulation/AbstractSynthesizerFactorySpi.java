package org.aksw.openqa.component.answerformulation;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractSynthesizerFactorySpi extends AbstractPluginFactorySpi<Synthesizer> implements SynthesizerFactorySpi {
}
