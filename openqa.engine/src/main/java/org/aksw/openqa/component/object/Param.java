package org.aksw.openqa.component.object;

import org.aksw.openqa.component.IComponent;

public class Param extends AbstractObject implements IParam {

	IComponent source;
	
	@Override
	public IComponent getSource() {
		return source;
	}
	
	public void setSource(IComponent source) {
		this.source = source;
	}
}
