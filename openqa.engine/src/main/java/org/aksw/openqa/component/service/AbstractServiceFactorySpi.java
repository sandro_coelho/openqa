package org.aksw.openqa.component.service;

import org.aksw.openqa.component.AbstractPluginFactorySpi;

public abstract class AbstractServiceFactorySpi extends AbstractPluginFactorySpi<Service> implements ServiceFactorySpi {
}
