package org.aksw.openqa.component.providers.impl;

import java.util.List;

import org.aksw.openqa.component.AbstractPluginProvider;
import org.aksw.openqa.component.service.Service;
import org.aksw.openqa.component.service.ServiceFactorySpi;

public class ServiceProvider extends AbstractPluginProvider<Service, ServiceFactorySpi> {
	public ServiceProvider(List<? extends ClassLoader> classLoaders) {
		super(ServiceFactorySpi.class, classLoaders, null);
	}
}
