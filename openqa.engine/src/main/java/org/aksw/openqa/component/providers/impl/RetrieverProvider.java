package org.aksw.openqa.component.providers.impl;

import java.util.List;

import org.aksw.openqa.component.ProcessProvider;
import org.aksw.openqa.component.answerformulation.Retriever;
import org.aksw.openqa.component.answerformulation.RetrieverFactorySpi;

public class RetrieverProvider extends ProcessProvider<Retriever, RetrieverFactorySpi> {
	public RetrieverProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(RetrieverFactorySpi.class, classLoaders, serviceProvider);
	}
}
