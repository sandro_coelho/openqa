package org.aksw.openqa.component.providers.impl;

import java.util.List;

import javax.servlet.ServletOutputStream;

import org.aksw.openqa.component.AbstractPluginProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.ui.render.InfoGraphRender;
import org.aksw.openqa.component.ui.render.InfoNode;
import org.aksw.openqa.component.ui.render.Render;
import org.aksw.openqa.component.ui.render.RenderFactorySpi;

public class RenderProvider extends AbstractPluginProvider<InfoGraphRender, RenderFactorySpi> implements Render {
	
	public RenderProvider(List<? extends ClassLoader> classLoaders, ServiceProvider serviceProvider) {
		super(RenderFactorySpi.class, classLoaders, serviceProvider);
	}
	
	public void render(RenderProvider renderProvider, InfoNode infoNode,
			Context runnerParams, ServletOutputStream out) throws Exception {
		List<InfoGraphRender> infoGraphs = renderProvider.list();
		for(InfoGraphRender infoGraph : infoGraphs) {
			if(infoGraph.isActive() && infoGraph.canRender(runnerParams, infoNode) && !infoGraph.contains("MAIN"))
				infoGraph.render(renderProvider, infoNode, runnerParams, out);
		}
	}

	public InfoGraphRender getRootRender() {
		List<InfoGraphRender> infoGraphs = list();
		for(InfoGraphRender infoGraph : infoGraphs) {
			if(infoGraph.isActive() && infoGraph.contains("MAIN"))
				return infoGraph;
		}
		return null;
	}
}
