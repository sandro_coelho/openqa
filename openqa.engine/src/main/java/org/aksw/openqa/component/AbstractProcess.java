package org.aksw.openqa.component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.apache.log4j.Logger;

public abstract class AbstractProcess extends AbstractPlugin implements IProcess {
	
	private static Logger logger = Logger.getLogger(AbstractProcess.class);
	
	public AbstractProcess(Class<? extends IPlugin> c, Map<String, Object> params) {
		super(c, params);
	}
	
	public java.util.List<? extends IResult> process(java.util.List<? extends IParam> params, ServiceProvider serviceProvider, Context context) throws Exception {
		List<IResult> results = new ArrayList<IResult>();
		for(IParam param : params) {
			try {
				results.addAll(process(param, serviceProvider, context));
			} catch (Exception e) {
				logger.error("Error processing token.", e);
			}
		}
		return results;
	}
	
	public boolean canProcess(List<? extends IParam> params) {
		// verify if can process all the params,		
		for(IParam param : params) {
			// if there is any param in the list that can not be processed, return false
			if(!canProcess(param))
				return false;
		}
		return true;
	}
}
