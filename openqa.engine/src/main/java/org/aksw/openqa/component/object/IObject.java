package org.aksw.openqa.component.object;

import java.util.Map;

public interface IObject {
	public Object getParam(String attr);
	public void setParam(String attr, Object value);
	
	public Map<String, Object> getParams();
	public boolean contains(String attr);

}
