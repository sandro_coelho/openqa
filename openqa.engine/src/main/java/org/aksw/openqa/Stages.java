package org.aksw.openqa;


public class Stages {

	// input process
	public final static String INTERPRETATION = "RESOLUTION";
	public final static String RETRIEVAL = "RETRIEVAL";
	public final static String SYNTHESIZATION = "SYNTHESIZATION";
	public final static String RESOLUTION = "RESOLUTION";
	
	// interface
	public final static String RENDER = "RENDER";
	
}
