package org.aksw.openqa.view.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ActionEvent;

import org.aksw.openqa.manager.log.ConsoleLog;
import org.aksw.openqa.manager.log.ConsoleLog.MemoryMeasure;
import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;

@ManagedBean(name="engineLogConsoleController")
@SessionScoped
public class EngineLogViewConsole implements Serializable {	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4523518673061012743L;	
	private static Logger logger = Logger.getLogger(EngineLogViewConsole.class);
	
	private String log;
	private CartesianChartModel linearMemoryConsumeModel;
	private LineChartSeries memorySeries;
	
	public EngineLogViewConsole() {
	}

	@PostConstruct
	public void init(){
		this.linearMemoryConsumeModel =  new CartesianChartModel();
		memorySeries = new LineChartSeries();
		memorySeries.setLabel("Memory (MB)");
		MemoryMeasure[] memoryMeasures = ConsoleLog.getInstance().getLastLogMemoryMeasures();
		for(MemoryMeasure memoryMeasure : memoryMeasures) {
			memorySeries.set(memoryMeasure.date, memoryMeasure.memory);
		}		
        linearMemoryConsumeModel.addSeries(memorySeries);
	}
		
	public String getLog() {
		String[] messages = ConsoleLog.getInstance().getLastLogMessages(); 
		log= "";
		for(String message : messages) {
			log += message;
		}		
		return log;
	}
	
	public void cleanLogMessages(ActionEvent event) {
		ConsoleLog.getInstance().clearLogMessages();
	}
	
	public void cleanmemoryConsumeLog(ActionEvent event) {
		ConsoleLog.getInstance().clearMemoryConsumeLog();
	}
	
	public void setLog(String value) {
		log = value;
	}
	
	public StreamedContent getFile() {
		InputStream stream = null;
		try {
			stream = new FileInputStream(ConsoleLog.getInstance().getLoggerFile());
		} catch (FileNotFoundException e) {
			logger.error("Can not find the logger file", e);
		}		
        return new DefaultStreamedContent(stream, "text/plain", ConsoleLog.LOG_FILE_NAME);
    }
	
	public CartesianChartModel getLinearMemoryConsumeModel() {
		return linearMemoryConsumeModel;
	}  
}
