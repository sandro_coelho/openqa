package org.aksw.openqa.runner;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.component.AbstractPluginProvider;
import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Param;
import org.aksw.openqa.component.providers.impl.InterpreterProvider;
import org.aksw.openqa.component.providers.impl.ResolverProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.aksw.openqa.process.AbstractProcess;
import org.aksw.openqa.process.AbstractStage;

public class QueryProcessorv2 extends AbstractProcess<IResult, IParam> {

//	public QueryResult process(Map<String, Object> params,
//			Map<Class<? extends IProvider<? extends IComponent>>, 
//			ComponentProvider<? extends IComponent>> providers,
//			Context context) throws Exception {
//		
//		Param param = new Param();
//		
//		// param set
//		for(Entry<String, Object> entry : params.entrySet())
//			param.setParam(entry.getKey(), entry.getValue());
//		
//		//exec(stages, param, stageExecutor, providers, context);
//
//
//		ServiceProvider serviceProvider = (ServiceProvider) providers.get(ServiceProvider.class);
//		
//		QueryResult result = new QueryResult();
//				
//		
//		
//		
//		// Input Interpreter
//		InterpreterProvider interpreterProvider = (InterpreterProvider) providers.get(InterpreterProvider.class);
////		List<? extends IParam> interpretations = interpreterProvider.process(token, serviceProvider, context);
//		
//		
//		// set SPARQL queries
//		result.setParam(QueryResult.SPARQL_QUERIES, interpretations);
//		
//	
//		// Processing
//		RetrieverProvider retrieverProvider = (RetrieverProvider) providers.get(RetrieverProvider.class);
//		List<? extends IParam> consultResults = retrieverProvider.process(interpretations, serviceProvider, context);	
//	
//		// Synthesizing
//		SynthesizerProvider synthesizerProvider = (SynthesizerProvider) providers.get(SynthesizerProvider.class);
//		List<? extends IParam> synthesesResults = synthesizerProvider.process(consultResults, serviceProvider, context);
//		
//		// Resolving
//		ResolverProvider resolverProvider = (ResolverProvider) providers.get(ResolverProvider.class);
//		List<? extends IResult> resolvedResults = resolverProvider.process(synthesesResults, serviceProvider, context);
//	
//		// set SPARQL results
//		result.setParam(QueryResult.SPARQL_RESULT, resolvedResults);
//		
//		return result;
//	}
	
//	private class AnswerFormulationStage<L extends IParam> extends AbstractStage<L, IParam> {
//
//		@Override
//		public List<L> exec(List<IParam> input, Object... staticArgs) throws Exception {
//			Map<Class<? extends IProvider<? extends IComponent>>,
//					AbstractComponentProvider<? extends IComponent>> providers = (Map<Class<? extends IProvider<? extends IComponent>>, AbstractComponentProvider<? extends IComponent>>) staticArgs[0];
//			Context context = (Context) staticArgs[1];
//			
//			ServiceProvider serviceProvider = (ServiceProvider) providers.get(ServiceProvider.class);
//			InterpreterProvider interpreterProvider = (InterpreterProvider) providers.get(InterpreterProvider.class);
//			List<L> interpretations = (List<L>) interpreterProvider.process(input, serviceProvider, context);
//			return interpretations;
//		}
//		
//	}
}
