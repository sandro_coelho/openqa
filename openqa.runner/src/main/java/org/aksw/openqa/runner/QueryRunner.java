package org.aksw.openqa.runner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.aksw.openqa.DefaultFrameworkParams;
import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.context.impl.DefaultContext;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.providers.impl.ContextProvider;
import org.aksw.openqa.manager.plugin.PluginManager;
import org.aksw.openqa.qald.schema.Answer;
import org.aksw.openqa.qald.schema.Answers;
import org.aksw.openqa.qald.schema.Dataset;
import org.aksw.openqa.qald.schema.Question;
import org.aksw.openqa.util.JAXBUtil;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

public class QueryRunner {
	
	private static Logger logger = Logger.getLogger(QueryRunner.class);
	
	private static final String OUT_OF_SCOPE_MESSAGE = " OUT OF SCOPE ";
	
	public Dataset evaluate(File qaldTest, String lang, PluginManager pluginManager) throws ParserConfigurationException, SAXException, IOException, JAXBException {

		Dataset dataset = JAXBUtil.deserialize(Dataset.class, qaldTest);
		
		QueryProcessor queryProcessor = new QueryProcessor();
		Map<String, Object> userIputParams = new HashMap<String, Object>();
		
		Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = null;
		try {
			providers = pluginManager.getProviders();
		} catch (MalformedURLException e) {
			logger.error("Error retrieving providers.", e);
		}
		
		// setting up the context
		ContextProvider contextProvider = (ContextProvider) providers.get(ContextProvider.class);
		Context context = contextProvider.get(DefaultContext.class);
		
		context.setParam(Context.REQUEST_HOST, "localhost");
		
		logger.debug("Running tests for dataset " + dataset.getId());
		
		List<Question> questions = dataset.getQuestion();
		
		for(Question question : questions) {
			List<org.aksw.openqa.qald.schema.String> stringElements = question.getString();
			for(org.aksw.openqa.qald.schema.String query : stringElements) {
				if(lang == null || query.getLang().equals(lang)) {
					String queryValue = query.getValue();
					logger.debug("Running query: " + queryValue);
					userIputParams.put(DefaultFrameworkParams.NL_INPUT_PARAM, queryValue);
					try {
						IResult result = queryProcessor.process(userIputParams,providers, context);
						@SuppressWarnings("unchecked")
						List<IResult> interpreterResults = (List<IResult>) result.getParam(QueryResult.SPARQL_QUERIES);
						String sparqlQueriesAnswer = null;
						if(interpreterResults != null && interpreterResults.size() > 0) {
							for(IResult interpreterResult : interpreterResults) {
								String sparqlParam = (String) interpreterResult.getParam(DefaultFrameworkParams.SPARQL_PARAM);
								if(sparqlParam != null) {									
									if(sparqlQueriesAnswer != null)
										sparqlQueriesAnswer += sparqlParam + ";";
									else
										sparqlQueriesAnswer = sparqlParam + ";";
								}
							}
						}
						
						if(sparqlQueriesAnswer == null)
							sparqlQueriesAnswer = QueryRunner.OUT_OF_SCOPE_MESSAGE;
						
						question.setQuery(sparqlQueriesAnswer);
						@SuppressWarnings("unchecked")
						List<IResult> entryResults = (List<IResult>) result.getParam(QueryResult.SPARQL_RESULT);
						if(entryResults != null) {
							question.setAnswers(getAnswers(entryResults));
						}
					} catch (Exception e) {
						logger.error("Erro executing query: " + queryValue, e);
					}
				}
			}
		}
		
		return dataset;
	}
	
	public String evaluate(String question, PluginManager pluginManager) throws ParserConfigurationException, SAXException, IOException, JAXBException {
		logger.debug("Running query: " + question);
		Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers = null;
		try {
			providers = pluginManager.getProviders();
		} catch (MalformedURLException e) {
			logger.error("Error retrieving providers.", e);
		}
		
		// setting up the context
		ContextProvider contextProvider = (ContextProvider) providers.get(ContextProvider.class);
		Context context = contextProvider.get(DefaultContext.class);		
		context.setParam(Context.REQUEST_HOST, "localhost");
		QueryProcessor queryProcessor = new QueryProcessor();
		String sparqlQueriesAnswer = null;
		IResult result;
		try {
			result = queryProcessor.process(question, providers, context);
			
			@SuppressWarnings("unchecked")
			List<IResult> interpreterResults = (List<IResult>) result.getParam(QueryResult.SPARQL_QUERIES);
			
			if(interpreterResults != null && interpreterResults.size() > 0) {
				for(IResult interpreterResult : interpreterResults) {
					String sparqlParam = (String) interpreterResult.getParam(DefaultFrameworkParams.SPARQL_PARAM);
					if(sparqlParam != null) {									
						if(sparqlQueriesAnswer != null)
							sparqlQueriesAnswer += sparqlParam + ";";
						else
							sparqlQueriesAnswer = sparqlParam + ";";
					}
				}
			}			
		} catch (Exception e) {
			logger.error("Error running query: " + question, e);
		}
		
		if(sparqlQueriesAnswer == null)
			return QueryRunner.OUT_OF_SCOPE_MESSAGE;
		
		return sparqlQueriesAnswer;
	}
	
	public Answers getAnswers(List<IResult> entryResults) {
		Answers answers = new Answers();
		for(IResult entryrResult : entryResults) {
			String attrValue = null;
			String attrKey = null;
			if(entryrResult.contains(DefaultFrameworkParams.URI_PARAM)) {
				attrValue = (String) entryrResult.getParam(DefaultFrameworkParams.URI_PARAM);
				attrKey = DefaultFrameworkParams.URI_PARAM;
			} else if (entryrResult.contains(DefaultFrameworkParams.LITERAL_NUMBER_PARAM)) {
				attrValue = (String) entryrResult.getParam(DefaultFrameworkParams.LITERAL_NUMBER_PARAM);
				attrKey = DefaultFrameworkParams.LITERAL_NUMBER_PARAM;
			} else if (entryrResult.contains(DefaultFrameworkParams.LITERAL_DATE_PARAM)) {
				attrValue = (String) entryrResult.getParam(DefaultFrameworkParams.LITERAL_DATE_PARAM);
				attrKey = DefaultFrameworkParams.LITERAL_DATE_PARAM;
			} else if (entryrResult.contains(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM)) {
				attrValue = (String) entryrResult.getParam(DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM);
				attrKey = DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM;
			} else {
				attrValue = (String) entryrResult.getParam(DefaultFrameworkParams.RESOURCE_PARAM);
				attrKey = DefaultFrameworkParams.RESOURCE_PARAM;
			}
			
			Answer answer = null;
			if(attrKey ==  DefaultFrameworkParams.URI_PARAM) {
				answer = new Answer();
				answer.setUri(attrKey);
				answers.getAnswer().add(answer);
			} else if(attrKey == DefaultFrameworkParams.LITERAL_DATE_PARAM) {
				answer = new Answer();
				answer.setDate(attrValue);
			} else if(attrKey == DefaultFrameworkParams.LITERAL_NUMBER_PARAM) {
				answer = new Answer();
				answer.setNumber(attrValue);
			} else if(attrKey == DefaultFrameworkParams.LITERAL_BOOLEAN_PARAM) {
				answer = new Answer();
				answer.setBoolean(attrValue);
			} else {
				answer = new Answer();
				answer.setUri(attrValue);
			}
			
			if(answer != null)
				answers.getAnswer().add(answer);
		}
		return answers;
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, URISyntaxException, JAXBException {
		if(args.length != 4) {
			System.out.println("Use java -jar testrunner <pluginDir> <QALDtestfile|question> <outputfile> <lang>");
			System.out.println("where:");
			System.out.println("pluginDir		-	Plugin directory.");
			System.out.println("QALDTestFile		-	QALD test file.");
			System.out.println("question		-	A cotated question (keywords or full question) e.g.\"Ho was the discover Brasil?\".");
			System.out.println("outputFile		-	destination output file.");
			System.out.println("lang			-	language to test (en,de,es,it,fr,nl,ro).");
			return;
		}

		File pluginDir = new File(args[0]);

		String arg1 = args[1];
		File qaldFile = new File(arg1);
		File outputFile = new File(args[2]);

		QueryRunner ev = new QueryRunner();
		ClassLoader contextClassLoader = ev.getClass().getClassLoader();
    	PluginManager pluginManager = new PluginManager(pluginDir.getAbsolutePath(), contextClassLoader);
    	pluginManager.setActivate(true);

    	if(qaldFile.isFile()) {
    		Dataset dataset = ev.evaluate(qaldFile, args[3], pluginManager);
			FileOutputStream fout = new FileOutputStream(outputFile);
			JAXBUtil.serialize(Dataset.class, dataset, fout, new String[] {"^string","^keywords","^query" });
    	} else {
    		System.out.println(ev.evaluate(arg1, pluginManager));
    	}
	}
}
