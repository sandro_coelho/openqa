package org.aksw.openqa.runner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.aksw.openqa.DefaultFrameworkParams;
import org.aksw.openqa.component.IPlugin;
import org.aksw.openqa.component.IProvider;
import org.aksw.openqa.component.context.Context;
import org.aksw.openqa.component.object.IParam;
import org.aksw.openqa.component.object.IResult;
import org.aksw.openqa.component.object.Param;
import org.aksw.openqa.component.providers.impl.InterpreterProvider;
import org.aksw.openqa.component.providers.impl.ResolverProvider;
import org.aksw.openqa.component.providers.impl.RetrieverProvider;
import org.aksw.openqa.component.providers.impl.ServiceProvider;
import org.aksw.openqa.component.providers.impl.SynthesizerProvider;
import org.apache.log4j.Logger;

public class QueryProcessor {
	
	private static Logger logger = Logger.getLogger(QueryProcessor.class);
	
	
	public QueryResult process(String quetion, 	
			Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers,
			Context context) throws Exception {
		Map<String, Object> userIputParams = new HashMap<String, Object>();
		userIputParams.put(DefaultFrameworkParams.NL_INPUT_PARAM, quetion);
		return process(userIputParams, providers, context);
	}
	
	public QueryResult process(Map<String, Object> params,
			Map<Class<? extends IProvider<? extends IPlugin>>, IProvider<? extends IPlugin>> providers,
			Context context) throws Exception {

		ServiceProvider serviceProvider = (ServiceProvider) providers.get(ServiceProvider.class);
		
		QueryResult result = new QueryResult();
				
		Param token = new Param();
		
		logger.info("Parameterizing");
		
		// param set
		for(Entry<String, Object> entry : params.entrySet())
			token.setParam(entry.getKey(), entry.getValue());
		
		logger.debug("Interpreting");
		// Input Interpreter
		InterpreterProvider interpreterProvider = (InterpreterProvider) providers.get(InterpreterProvider.class);
		List<? extends IParam> interpretations = interpreterProvider.process(token, serviceProvider, context);
		logger.debug("Number of interpretations: " + interpretations.size());		
		
		// set SPARQL queries
		result.setParam(QueryResult.SPARQL_QUERIES, interpretations);
		
		logger.debug("Retrieving ");
		// Processing
		RetrieverProvider retrieverProvider = (RetrieverProvider) providers.get(RetrieverProvider.class);
		List<? extends IParam> consultResults = retrieverProvider.process(interpretations, serviceProvider, context);	
		logger.debug("Number of consult results: " + consultResults.size());
		
		logger.debug("Synthesizing");
		// Synthesizing
		SynthesizerProvider synthesizerProvider = (SynthesizerProvider) providers.get(SynthesizerProvider.class);
		List<? extends IParam> synthesesResults = synthesizerProvider.process(consultResults, serviceProvider, context);
		logger.debug("Number of syntheses: " + synthesesResults.size());
		
		logger.debug("Resolving");
		// Resolving
		ResolverProvider resolverProvider = (ResolverProvider) providers.get(ResolverProvider.class);
		List<? extends IResult> resolvedResults = resolverProvider.process(synthesesResults, serviceProvider, context);
		logger.debug("Number of resolved results: " + resolvedResults.size());
		
		// set SPARQL results
		result.setParam(QueryResult.SPARQL_RESULT, resolvedResults);
		
		return result;
	}
}
