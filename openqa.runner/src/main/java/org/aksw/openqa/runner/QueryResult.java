package org.aksw.openqa.runner;

import org.aksw.openqa.component.object.Result;

public class QueryResult extends Result {
	public static final String SPARQL_QUERIES = "SPARQL_QUERIES";
	public static final String SPARQL_RESULT = "SPARQL_RESULT";	
}
